/*written by Akram Berkawy as a solution on Stackoverflow
	http://stackoverflow.com/questions/16345985/javascript-stylesheet-switcher-not-working */

function setActiveStyleSheet(title) {
	var i, a, main;
	
a = document.getElementsByTagName("link"); /*adds <link>s to a[] */
	for(i=0; i< a.length ; i++) { /*init loop*/
		if(a[i].getAttribute("rel").indexOf("style") != -1 /*checks stylesheets */
		&& a[i].getAttribute("title")) { /*checks passed var is stylesheet */
			a[i].disabled = true; /*end loop and go to set stylesheet*/
			if(a[i].getAttribute("title") == title) a[i].disabled = false; /*sets new document property */
    }
  }
}