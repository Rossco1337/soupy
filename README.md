Soupy
=====

Soupy is a static HTML5/CSS3 boilerplate designed to be compatible with all [compliant web browsers](https://www.youtube.com/watch?v=_DvEpWR66_8) and responsive for all types of displays.

Designed for simple websites with single-page content. The design focus is to keep most content centered on the page to allow maximum display usage for any type of display. Navigation is done through a single global navbar.

###Features
<ul>
<li>Fully relative positioning</li>
<li>Simple scalable navigation</li>
<li>Uses Google's Webfont API</li>
<li>Up to 3 column footers.</li>
</ul>

#####Preview of the latest commit can be viewed at:
######https://rawgit.com/Rossco1337/soupy/master/index.html
